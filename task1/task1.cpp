﻿#include <stdio.h>
#include <math.h>

unsigned long long factorial(int n) {
	if (n == 0) return 1;
	return n * factorial(n - 1);
}

int main()
{
	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = %d = %d\n",i, i, i);
	 }

	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = log(%d) = %lf\n", i, i, log10(i));
	}

	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = %d*log(%d) = %f\n", i, i, i, i * log10(i));
	}

	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = %d^2 = %d\n", i, i, i * i);
	}

	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = 2^%d = %lf\n", i, i, pow(2, i));
	}

	for (int i = 0; i <= 50; i++) {
		printf("f(%d) = %d! = %lld\n", i, i, factorial(i));
	}
}
