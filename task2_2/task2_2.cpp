﻿#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>

#define GETTIME std::chrono::high_resolution_clock::now()
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

std::vector<int> fibonacci(int n) {
	std::ofstream file("results.txt", std::ios::app);
	auto begin = GETTIME;
	std::vector<int> fib = { 0, 1 };
    for (int i = 2; i < n; i++) {
		fib.push_back(fib[i - 1] + fib[i - 2]);
	}
	auto end = GETTIME;
	int time = CALCTIME(end - begin).count();
	file << CALCTIME(end - begin).count() << std::endl;
	std::cout << "Time: " << CALCTIME(end - begin).count() << " ns\n";
	file.close();
    return fib;
}

int main()
{
	int n = 1;
	for (int i = 0; i <= 40; i++) {
		std::cout << "fib(" << i << ") = \n";
		std::vector<int> fib = fibonacci(i);
		for (size_t j = 0; j < fib.size(); j++)
		{
			std::cout << fib[j] << " ";
		}
		std::cout << std::endl;
	}
	return 0;
}