﻿#include <stdio.h>
#include <iostream>
#include <bitset>
#include <vector>
#include <chrono>
#include <fstream>
#include <time.h>

#define GETTIME std::chrono::high_resolution_clock::now()
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

std::string numToBinary(int n, int base) {
	std::ofstream file("results.txt", std::ios::app);
	auto begin = GETTIME;
	std::string binStr = "";
	int value = n;
	while (n > 1) {

		binStr.push_back(n % 2 + '0');
		n /= 2;
		base--;

	}
	for (int i = base; i >= 0; i--) {
		binStr.insert(binStr.begin(), 0 + '0');
	}
	auto end = GETTIME;
	auto time = CALCTIME(end - begin);
	file << time.count() << std::endl;
	std::cout << time.count() << std::endl;
	file.close();
	return binStr;

	return binStr;
}

int main()
{
	int a;
	int b;
	std::cout << "Enter a number: ";
	std::cin >> a;
	for (int i = 8; i <= 64; i++) {
		numToBinary(a, i);
	}
}

